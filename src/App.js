import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import './App.css';

function App() {
  return (
    <div className="App">
     <div>
        <section className='table'> {/* условная существующая верстка */}
          <p className="head">Firstname</p>
          <p className="head">Lastname</p>
          <p className="head">Age</p>
          <p>Jill</p>
          <p>Smith</p>
          <p>50</p>
          <p>Eve</p>
          <p>Jackson</p>
          <p>94</p>
        </section>
        <table id="table-to-xls"> {/* таблица для скачивания display: none */}
          <tr>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Age</th>
          </tr>
          <tr>
            <td>Jill</td>
            <td>Smith</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Eve</td>
            <td>Jackson</td>
            <td>94</td>
          </tr>
        </table>
        <ReactHTMLTableToExcel
          id="test-table-xls-button"
          className="download-table-xls-button"
          table="table-to-xls"
          filename="tablexls"
          sheet="tablexls"
          buttonText="Download as XLS"
        />
      </div>
    </div>
  );
}

export default App;
